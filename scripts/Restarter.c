#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>
#include <mpi.h>

#define DIR "/misc/work272/inouesg/work/NewVoidGal/Rockstar-0.99.9-RC3"

#define INBASE "/glv0/inouesg/NewVoidGal/Output_NewVoid_LBox/" // directory to snapshot files
#define OUTBASE "./Output_NewVoidGal_LBox" // where to output files

int NProcs, MyID;

int main(int argc, char *argv[])
{

  MPI_Init(&argc,&argv);
  MPI_Comm_size(MPI_COMM_WORLD,&NProcs);
  MPI_Comm_rank(MPI_COMM_WORLD,&MyID);

  char ConfigFileMade[128];
  if(MyID==0){
    mkdir(OUTBASE,0755);
    sprintf(ConfigFileMade,"%s/rockstar_param_res.cfg",OUTBASE);
    FILE *fp;
    if(!(fp = fopen(ConfigFileMade,"w"))){
      fprintf(stderr,"can't open out-file `%s`\n", ConfigFileMade);
      exit(0);
    }
    
    FILE *fptest;
    char words[100],fn[128];
    sprintf(fn,"%s/restart.cfg",OUTBASE);
    fptest = fopen(fn, "r");
    int status = 0;
    int c = 0;
    while(status!=EOF){
      status = fscanf(fptest, "%s", words);
      if(words[1]==44 || words[2]==44){
	if(status!=EOF) fprintf(fp,"%s", words);
      }else{
	c++;
	if(status!=EOF) fprintf(fp,"%s", words);
      }
      if(c==3){
	if(status!=EOF) fprintf(fp,"\n");
	c = 0;
      }else{
	if(status!=EOF) fprintf(fp," ");
      }
      
    }
    fclose(fptest);
    fclose(fp);

    fprintf(stderr,"\nFinding halos...");
    fprintf(stderr,"Parallel configuration\n");
    fprintf(stderr,"Output: %s\n",OUTBASE); 
  }

  MPI_Barrier(MPI_COMM_WORLD);
  
  char passbuf[256];
  sprintf(passbuf,"%s/rockstar -c %s/%s",DIR,DIR,ConfigFileMade);
  
  if(MyID==0){
    fprintf(stderr,"%s\n",passbuf);
    system(passbuf);
  }
  if(MyID==1){
    sprintf(passbuf,"%s/rockstar -c %s/auto-rockstar.cfg",DIR,OUTBASE);
    fprintf(stderr,"%s\n",passbuf);
    sleep(5);
    system(passbuf);
  }
  
  MPI_Barrier(MPI_COMM_WORLD);
  MPI_Finalize();
  fprintf(stderr,"end.\n");
  
  return 0;
}
