#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <unistd.h>

#define DIR "/mnt/synology1/takashi/mw/mw2_h68_dm_lv12/rockstar/"

#define INBASE "/mnt/synology1/takashi/mw/mw2_h68_dm_lv12/data/"// directory to snapshot files
#define OUTBASE "./halos" // where to output files

#define FILE_FORMAT "GIZMO"
#define GIZMO_LENGTH_CONVERSION 1.e-3
#define GIZMO_MASS_CONVERSION 1e+10
#define FORCE_RES 0.00003531497834338388       // Mpc/h

#define PARALLEL_IO  1
#define NUM_BLOCKS 32
#define NUM_SNAPS 70
#define STARTING_SNAP  0
#define FILENAME   "snapdir_<snap>/mw1_h68_dm_lv12_<snap>.<block>.hdf5"
#define NUM_WRITERS  8
#define FORK_READERS_FROM_WRITERS  1
#define FORK_PROCESSORS_PER_MACHINE  1
//#define OVERLAP_LENGTH 1.5
//#define MIN_HALO_OUTPUT_SIZE 32


int main(int argc, char *argv[])
{


  char ConfigFileMade[256];
  mkdir(OUTBASE,0755);
  sprintf(ConfigFileMade,"%s/rockstar_param.cfg",OUTBASE);
  FILE *fp;
  if(!(fp = fopen(ConfigFileMade,"w"))){
    fprintf(stderr,"can't open out-file `%s`\n", ConfigFileMade);
    exit(0);
  }

  fprintf(fp,"FILE_FORMAT = \"%s\"\n",FILE_FORMAT);
  fprintf(fp,"GIZMO_LENGTH_CONVERSION = %e\n",(double)GIZMO_LENGTH_CONVERSION);
  fprintf(fp,"GIZMO_MASS_CONVERSION = %.1lf\n",(double)GIZMO_MASS_CONVERSION);
  fprintf(fp,"FORCE_RES = %lf\n",(double)FORCE_RES);
  fprintf(fp,"OUTBASE = %s\n",OUTBASE);
#ifdef OVERLAP_LENGTH
  fprintf(fp,"OVERLAP_LENGTH = %lf\n",OVERLAP_LENGTH);
#endif
#ifdef MIN_HALO_OUTPUT_SIZE
  fprintf(fp,"MIN_HALO_OUTPUT_SIZE = %d\n",MIN_HALO_OUTPUT_SIZE);
#endif

  fprintf(fp,"PARALLEL_IO = %d\n",PARALLEL_IO);
  fprintf(fp,"INBASE = %s\n",INBASE);
  fprintf(fp,"NUM_BLOCKS = %d\n",NUM_BLOCKS);
  fprintf(fp,"NUM_SNAPS = %d\n",NUM_SNAPS);
  fprintf(fp,"STARTING_SNAP = %d\n",STARTING_SNAP);
  fprintf(fp,"FILENAME = %s\"\n",FILENAME);
  fprintf(fp,"NUM_WRITERS = %d\n",NUM_WRITERS);
  fprintf(fp,"FORK_READERS_FROM_WRITERS = %d\n",FORK_READERS_FROM_WRITERS);
  fprintf(fp,"FORK_PROCESSORS_PER_MACHINE = %d\n",FORK_PROCESSORS_PER_MACHINE);
  
  fclose(fp);

  fprintf(stderr,"\nFinding halos...");
  fprintf(stderr,"Parallel configuration\n");
  fprintf(stderr,"Output: %s\n",OUTBASE); 

  
  char passbuf[256];
  sprintf(passbuf,"mpiexec -n 1 %s/rockstar-galaxies -c %s/%s &",DIR,DIR,ConfigFileMade);
  
  fprintf(stderr,"%s\n",passbuf);
  system(passbuf);

  sprintf(passbuf,"mpiexec -n %d %s/rockstar-galaxies -c %s/auto-rockstar.cfg",NUM_WRITERS, DIR,OUTBASE);
  fprintf(stderr,"%s\n",passbuf);
  sleep(5);
  system(passbuf);

  fprintf(stderr,"end.\n");
  
  return 0;
}
