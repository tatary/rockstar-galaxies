/*
 * GIZMO I/O for Rockstar
 * Takshi Okamoto (okamoto@astro1.sci.hokudai.ac.jp)
 */
#ifdef ENABLE_HDF5

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>
#include <assert.h>
#include <string.h>
#include <hdf5.h> /* HDF5 required */
#include "io_hdf5.h"
#include "io_gizmo.h"
#include "io_util.h"
#include "../universal_constants.h"
#include "../check_syscalls.h"
#include "../config_vars.h"
#include "../config.h"
#include "../particle.h"

#define GIZMO_NTYPES 6

void gizmo_read_dataset(hid_t HDF_FileID, char *filename, char *gid, char *dataid, struct particle *p, int64_t to_read, int64_t offset, int64_t stride, hid_t type) {
  int64_t width = (type == H5T_NATIVE_LLONG) ? 8 : 4;
  if (type == H5T_NATIVE_UINT64) 
    width = 8; 
  void *buffer = check_malloc_s(buffer, to_read, width*stride);
  int64_t *ibuffer = buffer;
  float *fbuffer = buffer;
  int32_t *sbuffer = buffer; 

  hid_t HDF_GroupID = check_H5Gopen(HDF_FileID, gid, filename);
  hid_t HDF_DatasetID = check_H5Dopen(HDF_GroupID, dataid, gid, filename);
  hid_t HDF_DataspaceID = check_H5Dget_space(HDF_DatasetID);

  check_H5Sselect_all(HDF_DataspaceID);
  hssize_t npoints = H5Sget_select_npoints(HDF_DataspaceID);

  if (npoints != to_read*stride) {
    fprintf(stderr, "[Error] dataspace %s/%s in HDF5 file %s not expected size!\n  (Actual size = %"PRId64" elements; expected size = %"PRId64" elements\n", 
            gid, dataid, filename, (int64_t)(npoints), stride*to_read);
    exit(1);
  }

  check_H5Dread(HDF_DatasetID, type, buffer, dataid, gid, filename);
  
  H5Sclose(HDF_DataspaceID);
  H5Dclose(HDF_DatasetID);
  H5Gclose(HDF_GroupID);

  if (width == 8)
    for (int64_t i=0; i<to_read; i++)
      p[i].id = ibuffer[i];
  else if (type == H5T_NATIVE_UINT32) 
    for (int64_t i=0; i<to_read; i++)
      memcpy(((char *)&(p[i]))+offset, sbuffer+(i*stride), stride*width);
  else 
    for (int64_t i=0; i<to_read; i++)
      memcpy(((char *)&(p[i]))+offset, fbuffer+(i*stride), stride*width);

  free(buffer);
}

float gizmo_readheader_float(hid_t HDF_GroupID, char *filename, char *objName)
{
  char *gid = "Header";
  hid_t HDF_Type = H5T_NATIVE_FLOAT;
  hid_t HDF_AttrID = check_H5Aopen_name(HDF_GroupID, objName, gid, filename);
  hid_t HDF_DataspaceID = check_H5Aget_space(HDF_AttrID);

  check_H5Sselect_all(HDF_DataspaceID);
  
  float data = 0.0;
  check_H5Aread( HDF_AttrID, HDF_Type, &data, objName, gid, filename);

  H5Sclose(HDF_DataspaceID);
  H5Aclose(HDF_AttrID);
  return data;
}


void gizmo_readheader_array(hid_t HDF_GroupID, char *filename, char *objName, hid_t type, void *data)
{
  char *gid = "Header";
  hid_t HDF_AttrID = check_H5Aopen_name(HDF_GroupID, objName, gid, filename);
  hid_t HDF_DataspaceID = check_H5Aget_space(HDF_AttrID);
  check_H5Sselect_all(HDF_DataspaceID);

  int64_t ndims = check_H5Sget_simple_extent_ndims( HDF_DataspaceID );
  assert(ndims == 1);
  hsize_t dimsize = 0;
  check_H5Sget_simple_extent_dims(HDF_DataspaceID, &dimsize);
  assert(dimsize == GIZMO_NTYPES);
  
  check_H5Aread(HDF_AttrID, type, data, objName, gid, filename);

  H5Sclose(HDF_DataspaceID);
  H5Aclose(HDF_AttrID);
}

void gizmo_rescale_particles(struct particle *p, int64_t p_start, int64_t nelems) {
  double vel_rescale = sqrt(SCALE_NOW)*GIZMO_VELOCITY_CONVERSION;
  if (LIGHTCONE) vel_rescale = 1;
        
  for (int64_t i=0; i<nelems; i++) {
    for (int64_t j=0; j<3; j++) {
      p[p_start+i].pos[j]   *= GIZMO_LENGTH_CONVERSION;
      p[p_start+i].pos[j+3] *= vel_rescale;
    }
    p[p_start+i].mass *= GIZMO_MASS_CONVERSION;
  }
}

void load_particles_gizmo(char *filename, struct particle **p, int64_t *num_p)
{       
  hid_t HDF_FileID = check_H5Fopen(filename, H5F_ACC_RDONLY);
  hid_t HDF_Header = check_H5Gopen(HDF_FileID, "Header", filename);
  
  Ol = gizmo_readheader_float(HDF_Header, filename, "Omega_Lambda");
  Om = gizmo_readheader_float(HDF_Header, filename, "Omega_Matter");
  h0 = gizmo_readheader_float(HDF_Header, filename, "HubbleParam");
  SCALE_NOW = gizmo_readheader_float(HDF_Header, filename, "Time");
  BOX_SIZE = gizmo_readheader_float(HDF_Header, filename, "BoxSize");
  BOX_SIZE *= GIZMO_LENGTH_CONVERSION;  
  
  uint32_t npart_low[GIZMO_NTYPES], npart_high[GIZMO_NTYPES] = {0};
  int64_t npart[GIZMO_NTYPES];
  float massTable[GIZMO_NTYPES];
  
  gizmo_readheader_array(HDF_Header, filename, "NumPart_ThisFile", H5T_NATIVE_UINT64, npart);
  gizmo_readheader_array(HDF_Header, filename, "NumPart_Total_HighWord", H5T_NATIVE_UINT32, npart_high);
  gizmo_readheader_array(HDF_Header, filename, "NumPart_Total", H5T_NATIVE_UINT32, npart_low);
  gizmo_readheader_array(HDF_Header, filename, "MassTable", H5T_NATIVE_FLOAT, massTable);
    
  H5Gclose(HDF_Header);
    
  PARTICLE_MASS   = massTable[GIZMO_DM_PARTTYPE] * GIZMO_MASS_CONVERSION;
  AVG_PARTICLE_SPACING = cbrt(PARTICLE_MASS / (Om*CRITICAL_DENSITY));
        
  if(RESCALE_PARTICLE_MASS)
    PARTICLE_MASS = Om*CRITICAL_DENSITY * pow(BOX_SIZE, 3) / TOTAL_PARTICLES;
 
  int64_t to_read = 0;
  TOTAL_PARTICLES = 0;
  for (int64_t i=0; i<GIZMO_NTYPES; i++) {
    to_read += npart[i];
    TOTAL_PARTICLES += ( ((int64_t)npart_high[i]) << 32 ) 
      + (int64_t)npart_low[i];
  }

  printf("GIZMO: filename:       %s\n", filename);
  printf("GIZMO: box size:       %g Mpc/h\n", BOX_SIZE);
  printf("GIZMO: h0:             %g\n", h0);
  printf("GIZMO: scale factor:   %g\n", SCALE_NOW);
  printf("GIZMO: Total Part:     %" PRIu64 "\n", TOTAL_PARTICLES);
  printf("GIZMO: ThisFile Part:  %" PRIu64 "\n", to_read);
  printf("GIZMO: DM Part Mass:   %g Msun/h\n", PARTICLE_MASS);
  printf("GIZMO: avgPartSpacing: %g Mpc/h\n\n", AVG_PARTICLE_SPACING);
  
  if (!npart[GIZMO_DM_PARTTYPE]) {
    H5Fclose(HDF_FileID);
    printf("   SKIPPING FILE, PARTICLE COUNT ZERO.\n");
    return;
  }

  check_realloc_s(*p, ((*num_p)+to_read), sizeof(struct particle));
  memset((*p)+(*num_p), 0, sizeof(struct particle)*to_read);

  for (int64_t i=0; i<GIZMO_NTYPES; i++) {
    // read IDs, pos, vel
    char buffer[100];
    int32_t type = RTYPE_DM;
    if (i==4) type = RTYPE_STAR;
    else if (i==0) type = RTYPE_GAS;
    else if (i==5) type = RTYPE_BH; 
    if (!npart[i]) continue;

    snprintf(buffer, 100, "PartType%"PRId64, i);
    if (GIZMO_ID_BYTES == 4) { 
      gizmo_read_dataset(HDF_FileID, filename, buffer, "ParticleIDs", *p + (*num_p),
           npart[i], (char *)&(p[0][0].id)-(char*)(p[0]), 1, H5T_NATIVE_UINT);
    } else {
      gizmo_read_dataset(HDF_FileID, filename, buffer, "ParticleIDs", *p + (*num_p),
           npart[i], (char *)&(p[0][0].id)-(char*)(p[0]), 1, H5T_NATIVE_ULLONG);
    }
    gizmo_read_dataset(HDF_FileID, filename, buffer, "Coordinates", *p + (*num_p),
           npart[i], (char *)&(p[0][0].pos[0])-(char*)(p[0]), 3, H5T_NATIVE_FLOAT);
    gizmo_read_dataset(HDF_FileID, filename, buffer, "Velocities", *p + (*num_p),
           npart[i], (char *)&(p[0][0].pos[3])-(char*)(p[0]), 3, H5T_NATIVE_FLOAT);
    if (!massTable[i]) {
      gizmo_read_dataset(HDF_FileID, filename, buffer, "Masses", *p + (*num_p),
           npart[i], (char *)&(p[0][0].mass)-(char*)(p[0]), 1, H5T_NATIVE_FLOAT);
      /* if mass table is 0 but type is (primary) dark matter, need to set dark-matter particle mass */
      if (i == GIZMO_DM_PARTTYPE) {
        PARTICLE_MASS = p[0][*num_p].mass * GIZMO_MASS_CONVERSION;
        AVG_PARTICLE_SPACING = cbrt(PARTICLE_MASS / (Om*CRITICAL_DENSITY));
      }
    }
    else {
      for (int64_t j=0; j<npart[i]; j++) p[0][(*num_p)+j].mass = massTable[i];
    }
    for (int64_t j=0; j<npart[i]; j++) p[0][(*num_p)+j].type = type;

    /*if (type==RTYPE_GAS || type==RTYPE_STAR)
      gizmo_read_dataset(HDF_FileID, filename, buffer, "Metallicity", *p + (*num_p),
           npart[i], (char *)&(p[0][0].metallicity)-(char*)(p[0]), 1, H5T_NATIVE_FLOAT);
    */

    if (type==RTYPE_GAS) {
      gizmo_read_dataset(HDF_FileID, filename, buffer, "SmoothingLength", *p + (*num_p),
           npart[i], (char *)&(p[0][0].softening)-(char*)(p[0]), 1, H5T_NATIVE_FLOAT);

      gizmo_read_dataset(HDF_FileID, filename, buffer, "InternalEnergy", *p + (*num_p),
           npart[i], (char *)&(p[0][0].energy)-(char*)(p[0]), 1, H5T_NATIVE_FLOAT);
    }

    gizmo_rescale_particles(*p, *num_p, npart[i]);
    printf("GIZMO: first part, type %"PRId64": id = %"PRId64", (%f, %f, %f, %f, %f, %f); t=%d, u=%f, m=%e\n", i, p[0][*num_p].id, p[0][*num_p].pos[0], p[0][*num_p].pos[1], p[0][*num_p].pos[2], p[0][*num_p].pos[3], p[0][*num_p].pos[4], p[0][*num_p].pos[5], type, p[0][*num_p].energy, p[0][*num_p].mass);
    *num_p += npart[i];
    printf("GIZMO: DM Part Mass:   %g Msun/h\n", PARTICLE_MASS);
    printf("GIZMO: avgPartSpacing: %g Mpc/h\n\n", AVG_PARTICLE_SPACING);
  }

  H5Fclose(HDF_FileID);
}

#endif /* ENABLE_HDF5 */
